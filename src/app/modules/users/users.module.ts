import {NgModule} from "@angular/core";

import {UserListComponent} from "./views/user-list/user-list.component";
import {UserDetailComponent} from "./views/user-detail/user-detail.component";

@NgModule({
  declarations: [UserListComponent, UserDetailComponent],
})
export class UsersModule {

}
