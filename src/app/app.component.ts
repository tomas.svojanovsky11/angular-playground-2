import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";

// type User = {
//   id: string;
//   firstName: string;
//   lastName: string;
// }
//
// const defaultValues: User = {
//   id: "",
//   firstName: "",
//   lastName: "",
// };

type FormValues = {
  email: string;
  password: string;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  form = this.formBuilder.group({
    email: ["", [Validators.required, Validators.email]],
    password: ["", [Validators.required]],
  });

  isVisible = true;

  // Dependency injection
  constructor(private formBuilder: FormBuilder) {
  }

  onSubmit(formValues: FormValues) {
    console.log(formValues);
  }

  // myFirstText = "Toto je muj prvni text";
  //
  // defaultValues = defaultValues;
  //
  // users: User[] = [
  //   {
  //     id: 'xxx',
  //     firstName: "Endrej",
  //     lastName: "Babisch",
  //   },
  //   {
  //     id: 'yyy',
  //     firstName: "Milosz",
  //     lastName: "Z-man",
  //   },
  //   {
  //     id: 'zzz',
  //     firstName: "Nemam",
  //     lastName: "Zadne jmeno",
  //   },
  // ];
  //
  // newUser = defaultValues;
  //
  // onAddUser() {
  //   const { lastName, firstName, id } = this.newUser;
  //
  //   this.users = [...this.users, {
  //     id,
  //     firstName,
  //     lastName,
  //   }];
  //
  //   this.newUser = defaultValues;
  // }
  //
  // onFirstNameChange(event: KeyboardEvent) {
  //   const firstName = (event.target as HTMLInputElement).value;
  //   this.newUser = {
  //     ...this.newUser,
  //     firstName,
  //   };
  // }
  //
  // onLastNameChange(event: KeyboardEvent) {
  //   const lastName = (event.target as HTMLInputElement).value;
  //   this.newUser = {
  //     ...this.newUser,
  //     lastName,
  //   };
  // }
  //
  // trackByFn(_: number, user: User) {
  //   return user.id;
  // }
}
