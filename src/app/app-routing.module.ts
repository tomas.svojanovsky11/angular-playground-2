import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {UserListComponent} from "./modules/users/views/user-list/user-list.component";
import {UserDetailComponent} from "./modules/users/views/user-detail/user-detail.component";

const routes: Routes = [
  {
    path: 'users',
    component: UserListComponent,
  },
  {
    path: 'users/:id',
    component: UserDetailComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
